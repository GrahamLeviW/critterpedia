const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const critterSchema = new Schema({
  crittername: { type: String, required: true },
  description: { type: String, required: true },
  location: { type: String, required: true },
  image: { type: String },
  size: { type: String },
  kingdom: { type: String },
  phylum: { type: String },
  critterclass: { type: String },
  order: { type: String },
  family: { type: String },
  genus: { type: String },
  species: { type: String },
  binomialname: { type: String }
});

const Critter = mongoose.model("Critter", critterSchema);

module.exports = Critter;
