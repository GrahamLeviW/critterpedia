import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
//import Datepicker from "react-datepicker";

export default class editCritter extends Component {
  constructor(props) {
    super(props);

    this.onChangeCrittername = this.onChangeCrittername.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.onChangeSize = this.onChangeSize.bind(this);
    this.onChangeKingdom = this.onChangeKingdom.bind(this);
    this.onChangePhylum = this.onChangePhylum.bind(this);
    this.onChangeCritterclass = this.onChangeCritterclass.bind(this);
    this.onChangeOrder = this.onChangeOrder.bind(this);
    this.onChangeFamily = this.onChangeFamily.bind(this);
    this.onChangeGenus = this.onChangeGenus.bind(this);
    this.onChangeSpecies = this.onChangeSpecies.bind(this);
    this.onChangeBinomialname = this.onChangeBinomialname.bind(this);
    //this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      crittername: "",
      description: "",
      location: "",
      image: "",
      size: 0,
      kingdom: 0,
      phylum: "",
      critterclass: "",
      order: "",
      family: "",
      genus: "",
      species: "",
      binomialname: ""
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:5000/critters/" + this.props.match.params.id)
      .then(res => {
        this.setState({
          crittername: res.data.crittername,
          description: res.data.description,
          location: res.data.location,
          image: res.data.image,
          size: res.data.size,
          kingdom: res.data.kingdom,
          phylum: res.data.phylum,
          critterclass: res.data.critterclass,
          order: res.data.order,
          family: res.data.family,
          genus: res.data.genus,
          species: res.data.species,
          binomialname: res.data.binomialname
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  onChangeCrittername(e) {
    this.setState({
      crittername: e.target.value
    });
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  onChangeLocation(e) {
    this.setState({
      location: e.target.value
    });
  }

  onChangeImage(e) {
    this.setState({
      image: e.target.value
    });
  }

  onChangeSize(e) {
    this.setState({
      size: e.target.value
    });
  }
  onChangeKingdom(e) {
    this.setState({
      kingdom: e.target.value
    });
  }
  onChangePhylum(e) {
    this.setState({
      phylum: e.target.value
    });
  }
  onChangeCritterclass(e) {
    this.setState({
      critterclass: e.target.value
    });
  }
  onChangeOrder(e) {
    this.setState({
      order: e.target.value
    });
  }
  onChangeFamily(e) {
    this.setState({
      family: e.target.value
    });
  }
  onChangeGenus(e) {
    this.setState({
      genus: e.target.value
    });
  }
  onChangeSpecies(e) {
    this.setState({
      species: e.target.value
    });
  }
  onChangeBinomialname(e) {
    this.setState({
      binomialname: e.target.value
    });
  }

  //From when this was an edit page. Not in use, but still here for functionality if ever need be.
  /*   onSubmit(e) {
    e.preventDefault();
    const critter = {
      crittername: this.state.crittername,
      description: this.state.description,
      location: this.state.location,
      image: this.state.image,
      size: this.state.size,
      price: this.state.price,
      phylum: this.state.phylum,
      months: this.state.months,
      collectphrase: this.state.collectphrase
    };

    console.log(critter);

    axios
      .post(
        "http://localhost:5000/critters/update/" + this.props.match.params.id,
        critter
      )
      .then(res => console.log(res.data));
    //window.location = "/";
  } */

  render() {
    return (
      <div className="container-fluid">
        <h1 className="title">{this.state.crittername}</h1>
        <br />
        <div className="row">
          <div className="col-8">
            <h4 className="border-bottom border-dark">Description</h4>
            <p className="description">{this.state.description}</p>
            <h4 className="border-bottom border-dark">Common Habbitat</h4>
            <p className="description">{this.state.location}</p>
          </div>
          <div className="col-4">
            <img
              className="card-img"
              src={this.state.image}
              alt="critterImage"
            />

            <table class="table">
              <thread>
                <tr>
                  <th scope="col">Quick Facts</th>
                </tr>
              </thread>
              <tbody>
                <tr>
                  <th scope="row">Kingdom:</th>
                  <td>{this.state.kingdom}</td>
                </tr>
                <tr>
                  <th scope="row">Phylum:</th>
                  <td>{this.state.phylum}</td>
                </tr>
                <tr>
                  <th scope="row">Class:</th>
                  <td>{this.state.critterclass}</td>
                </tr>
                <tr>
                  <th scope="row">Order:</th>
                  <td>{this.state.order}</td>
                </tr>
                <tr>
                  <th scope="row">Family:</th>
                  <td>{this.state.family}</td>
                </tr>
                <tr>
                  <th scope="row">Genus:</th>
                  <td>{this.state.genus}</td>
                </tr>
                <tr>
                  <th scope="row">Species:</th>
                  <td>{this.state.species}</td>
                </tr>
              </tbody>
            </table>
            <Link to="/">
              <button type="button" className="btn btn-outline-secondary">
                Return to Browse
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
