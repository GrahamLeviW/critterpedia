import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

import Navbar from "./components/navbar";
import CrittersList from "./components/critterList";
import EditCritter from "./components/editCritter";
import CreateCritter from "./components/createCritter";
/* import CreateUser from "./components/createUser"; */

function App() {
  return (
    <html className="container-fluid-app">
      <Router>
        <Navbar />
        <Route path="/" exact component={CrittersList} />
        <Route path="/edit/:id" component={EditCritter} />
        <Route path="/create" component={CreateCritter} />
      </Router>
    </html>
  );
}

export default App;
