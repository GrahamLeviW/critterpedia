import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./components.css";

export default class navbar extends Component {
  render() {
    return (
      <nav className="nav navbar-expand-lg sticky-top">
        <Link to="/" className="navbar-brand">
          Critterpedia
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarTogglerDemo02"
          aria-controls="navbarTogglerDemo02"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="navbar-link">
              <Link to="/" className="nav-link">
                Critters
              </Link>
            </li>
            <li className="navbar-link">
              <Link to="/create" className="nav-link">
                Create Critter
              </Link>
            </li>
            {/* <li className="navbar-link">
              <Link to="/user" className="nav-link">
                Create User
              </Link>
            </li> */}
          </ul>
        </div>
      </nav>
    );
  }
}
