const router = require("express").Router();
let Critter = require("../models/critter.model");

router.route("/").get((req, res) => {
  Critter.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json(err));
});

router.route("/add").post((req, res) => {
  const crittername = req.body.crittername;
  const description = req.body.description;
  const location = req.body.location;
  const image = req.body.image;
  const size = req.body.size;
  const kingdom = req.body.kingdom;
  const phylum = req.body.phylum;
  const critterclass = req.body.critterclass;
  const order = req.body.order;
  const family = req.body.family;
  const genus = req.body.genus;
  const species = req.body.species;
  const binomialname = req.body.binomialname;

  const newCritter = new Critter({
    crittername,
    description,
    location,
    image,
    size,
    kingdom,
    phylum,
    critterclass,
    order,
    family,
    genus,
    species,
    binomialname
  });

  newCritter
    .save()
    .then(() => res.json("Critter added"))
    .catch(err => res.status(400).json(err));
});

router.route("/:id").get((req, res) => {
  Critter.findById(req.params.id)
    .then(critter => res.json(critter))
    .catch(err => res.status(400).json(err));
});

router.route("/:id").delete((req, res) => {
  Critter.findByIdAndDelete(req.params.id)
    .then(() => res.json("Critter deleted"))
    .catch(err => res.status(400).json("err"));
});

router.route("/update/:id").post((req, res) => {
  Critter.findById(req.params.id)
    .then(critter => {
      critter.crittername = req.body.crittername;
      critter.description = req.body.description;
      critter.location = req.body.location;
      critter.size = req.body.size;

      critter
        .save()
        .then(() => res.json("critter updated"))
        .catch(err => res.status(400).json(err));
    })
    .catch(err => res.status(400).json(err));
});

module.exports = router;
