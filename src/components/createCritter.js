import React, { Component } from "react";
import axios from "axios";
//import Datepicker from "react-datepicker";

export default class createCritter extends Component {
  constructor(props) {
    super(props);

    this.onChangeCrittername = this.onChangeCrittername.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.onChangeSize = this.onChangeSize.bind(this);
    this.onChangeKingdom = this.onChangeKingdom.bind(this);
    this.onChangePhylum = this.onChangePhylum.bind(this);
    this.onChangeCritterclass = this.onChangeCritterclass.bind(this);
    this.onChangeOrder = this.onChangeOrder.bind(this);
    this.onChangeFamily = this.onChangeFamily.bind(this);
    this.onChangeGenus = this.onChangeGenus.bind(this);
    this.onChangeSpecies = this.onChangeSpecies.bind(this);
    this.onChangeBinomialname = this.onChangeBinomialname.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      crittername: "",
      description: "",
      location: "",
      image: "",
      size: "",
      kingdom: "",
      phylum: "",
      critterclass: "",
      order: "",
      family: "",
      genus: "",
      species: "",
      binomialname: ""
    };
  }

  componentDidMount() {
    /* axios.get("http://localhost:5000/users/").then(res => {
      if (res.data.length > 0) {
        this.setState({
          users: res.data.map(user => user.username),
          crittername: res.data[0].username
        });
      }
    }); */
  }

  onChangeCrittername(e) {
    this.setState({
      crittername: e.target.value
    });
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  onChangeLocation(e) {
    this.setState({
      location: e.target.value
    });
  }

  onChangeImage(e) {
    this.setState({
      image: e.target.value
    });
  }

  onChangePrice(e) {
    this.setState({
      price: e.target.value
    });
  }

  onChangeSize(e) {
    this.setState({
      size: e.target.value
    });
  }

  onChangeKingdom(e) {
    this.setState({
      kingdom: e.target.value
    });
  }

  onChangePhylum(e) {
    this.setState({
      phylum: e.target.value
    });
  }
  onChangeCritterclass(e) {
    this.setState({
      critterclass: e.target.value
    });
  }
  onChangeOrder(e) {
    this.setState({
      order: e.target.value
    });
  }
  onChangeFamily(e) {
    this.setState({
      family: e.target.value
    });
  }
  onChangeGenus(e) {
    this.setState({
      genus: e.target.value
    });
  }
  onChangeSpecies(e) {
    this.setState({
      species: e.target.value
    });
  }
  onChangeBinomialname(e) {
    this.setState({
      binomialname: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const critter = {
      crittername: this.state.crittername,
      description: this.state.description,
      location: this.state.location,
      image: this.state.image,
      size: this.state.size,
      kingdom: this.state.kingdom,
      critterclass: this.state.critterclass,
      phylum: this.state.phylum,
      order: this.state.order,
      family: this.state.family,
      genus: this.state.genus,
      species: this.state.species,
      binomialname: this.state.binomialname
    };

    console.log(critter);

    axios
      .post("http://localhost:5000/critters/add", critter)
      .then(res => console.log(res.data));
    //window.location = "/";
  }

  render() {
    return (
      <div>
        <h3>Create New Critter Log</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>*Critter Name: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.crittername}
              onChange={this.onChangeCrittername}
            />
          </div>

          <div className="form-group">
            <label>*Description: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
            />
          </div>
          <div className="form-group">
            <label>Location: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.location}
              onChange={this.onChangeLocation}
            />
          </div>
          <div className="form-group">
            <label>*image-URL: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.image}
              onChange={this.onChangeImage}
            />
          </div>
          <div className="form-group">
            <label>Size: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.size}
              onChange={this.onChangeSize}
            />
          </div>
          <div className="form-group">
            <label>*Kingdom: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.kingdom}
              onChange={this.onChangeKingdom}
            />
          </div>
          <div className="form-group">
            <label>*Phylum: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.phylum}
              onChange={this.onChangePhylum}
            />
          </div>
          <div className="form-group">
            <label>*Class: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.cclass}
              onChange={this.onChangeCritterclass}
            />
          </div>
          <div className="form-group">
            <label>*Order: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.order}
              onChange={this.onChangeOrder}
            />
          </div>
          <div className="form-group">
            <label>*Family: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.family}
              onChange={this.onChangeFamily}
            />
          </div>
          <div className="form-group">
            <label>*Genus: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.genus}
              onChange={this.onChangeGenus}
            />
          </div>
          <div className="form-group">
            <label>*Species: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.species}
              onChange={this.onChangeSpecies}
            />
          </div>
          <div className="form-group">
            <label>Binomial Name: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.binomialname}
              onChange={this.onChangeBinomialname}
            />
          </div>

          <div className="form-group">
            <input
              type="submit"
              value="Create Critter Log"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}

/* <div className="form-group">
            <label>Crittername: </label>
            <select
              required
              className="form-control"
              value={this.state.crittername}
              onChange={this.onChangeCrittername}
            >
              {this.state.users.map(function(user) {
                return (
                  <option key={user} value={user}>
                    {user}
                  </option>
                );
              })}
            </select>
          </div> */
