import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./components.css";

//Code here will effect the individual cards within the entire section.

const CritterCard = props => (
  <div className="card">
    <Link className="hyperlink" to={"/edit/" + props.critter._id}>
      <img className="card-img-top" src={props.critter.image} alt="Critter" />
      <div className="card-body">
        <h5 className="card-title">{props.critter.crittername}</h5>
      </div>
    </Link>
  </div>
);

export default class critterList extends Component {
  constructor(props) {
    super(props);

    this.state = { critters: [] };
  }

  componentDidMount() {
    axios
      .get("http://localhost:5000/critters/")
      .then(res => {
        this.setState({ critters: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  deleteCritter(id) {
    axios
      .delete("http://localhost:5000/critters/" + id)
      .then(res => console.log(res.data));

    this.setState({
      critters: this.state.critters.filter(element => element._id !== id)
    });
  }

  critterList() {
    return this.state.critters.map(currentcritter => {
      return <CritterCard critter={currentcritter} key={currentcritter._id} />;
    });
  }

  //The overall grouping section of the cards above.
  render() {
    return (
      <div className="cardstack container-fluid">{this.critterList()}</div>
    );
  }
}
